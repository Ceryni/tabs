<section class="primary vertical-tabs">
		<div class="tab-content-holder">
			<div class="tab-content active" slug="support-services">
				<div class="container">
					<div class="row align-center align-middle">
						<div class="col-12 col-md-10 zero">
							<div class="tab-content-inner text-left fullWidth">
								<div class="tab-content-inner-colour text-center">
									<strong>Going alcohol free during pregnancy isn’t always easy.</strong>
									<p>Book an appointment with your midwife or GP to discuss going alcohol free safely. Alternatively contact one of our nearby specialist services or view support information below to find the right support plan for you.</p>
									<p>If you suffer from symptoms such as shaking, sweating or feeling sick and anxious before the first drink of the day, then you should seek medical advice before stopping drinking completely. It can be very dangerous to stop drinking without proper advice and support. </p>
									<strong class="contact toggleButton-js">
										Alcohol Services in Your Area
										<span class="fa fa-angle-down top"></span>
									</strong>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-content" slug="additional-services">
				<div class="container">
					<div class="row align-center align-middle">
						<div class="col-12 col-md-10 zero">
							<div class="tab-content-inner text-left fullWidth">
								<div class="tab-content-inner-colour text-center">
									<p>If you have any questions or comments about this campaign, you can contact the #DRYMESTER team at <a href="mailto:info@drymester.org.uk">info@drymester.org.uk</a></p>
									<p>If you've been affected by any of the information here and would like specialist help or support, get in touch with one of these dedicated services:</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>