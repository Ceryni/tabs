    /**
     * Load the tabs
     */
    loadTabs() {
        // holds all tabs's
        let tabs = $('.vertical-tabs');
        // define the button class
        let theButton = $('.tab-page-slug');
        // define the toggle container class
        let toggleContainer = $('.tab-content');
        // data attrib name
        let attrib = 'slug';

        // add active to the first button
        theButton.first().addClass('active');
        // add active to first tab content container
        toggleContainer.first().addClass('active');

        // if the element exists
        if(tabs.length) {
            // on click,
            theButton.on('click', ({currentTarget}) => {
                // remove the active class from all other buttons
                theButton.removeClass('active')
                //apply an active class to the button clicked
                $(currentTarget).addClass('active')
                // get data attrib of current active tab
                let currentAttrib = $(currentTarget).attr(attrib)
                // rmeove all active classes on the tab containers
                toggleContainer.removeClass('active');
                // filter all tab containers
                toggleContainer.filter(function () {
                    // of the attrib matches the the current active data attrib
                    // add a class of 'active'
        			return $(this).attr(attrib) === currentAttrib;
        		}).addClass('active');

            })
        }
    }